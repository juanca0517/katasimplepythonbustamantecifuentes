
class Calculo:
    def calcular(self,cadena):
        conteo=0
        minimo=0
        maximo=0
        promedio=0
        if cadena == "":
            return [0,0,0,0]

        valores = cadena.split(",")
        conteo = len(valores)
        numeros = [int(x) for x in valores]
        minimo = min(numeros)
        maximo = max(numeros)
        promedio = sum(numeros)/conteo
        respuesta=[conteo, minimo, maximo, promedio]
        return respuesta
