from unittest import TestCase

from Calculo import Calculo


class CalculoTest(TestCase):
    def test_calcularCadenaVacia(self):
        self.assertEqual(Calculo().calcular(""),[0,0,0,0],"Cadena Vacia")

    def test_CalcularUnSoloValor(self):
        self.assertEqual(Calculo().calcular("34"), [1,34,34,34], "Un solo numero")

    def test_calcularCadenaDosValores(self):
        self.assertEqual(Calculo().calcular("34,20"),[2,20,34,27],"Dos numeros")

    def test_CalcularNNumeros(self):
        self.assertEqual(Calculo().calcular("34,20,56,42"), [4,20,56,38], "N numeros")
